// SkillBox_18.5.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>
using namespace std;

class Player
{
private:
	std::string Name = "Name";
	int Score = 0;
public:
	std::string SetName()
{
		std::cout << "Enter Player Name: ";
		std::cin >> Name;
		return Name;
}
	int GetScore()
	{
		return Score;
	}
	int SetScore()
	{
		std::cout << "Enter Player Score: ";
		std::cin >> Score;
		return Score;
	}
	void PrintPlayer()
	{
		std::cout << "Name: " << Name << " Score: " << Score << "\n";
	}
	

};

void bubbleSort(Player List[], int ListLength)
{
	while (ListLength--)
	{
		bool swapped = false;

		for (int i = 0; i < ListLength; i++)
		{
			if (List[i].GetScore() < List[i + 1].GetScore())
			{
				swap(List[i], List[i + 1]);
				swapped = true;
			}
		}
		if (swapped = false)
			break;
	}

}





int main()
{
	int NumberOfPlayers;	
	
	std::cout << "How many players? ";
	std::cin >> NumberOfPlayers;
	
	Player* Players = new Player[NumberOfPlayers];
	
	
	for (int i = 0; i < NumberOfPlayers; i++)
	{
		Players[i].SetName();
		Players[i].SetScore();
	}
	bubbleSort(Players, NumberOfPlayers);

	std::cout << "\n" << "Sorted Array: " << "\n";
	for (int i = 0; i < NumberOfPlayers; i++)
	{
		Players[i].PrintPlayer();
	}
	delete[] Players;
}

